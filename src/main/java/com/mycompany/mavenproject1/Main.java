/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavenproject1;

/**
 *
 * @author caas1
 */
public class Main {
  public static void main(String[] args) {
    TaxCalculator calculator = new EmployeeCalculator();
    EmployeeTaxMenu menu = new EmployeeTaxMenu(calculator);
    menu.run();
  }
}