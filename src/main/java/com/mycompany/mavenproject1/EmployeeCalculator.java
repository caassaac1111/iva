/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavenproject1;

/**
 *
 * @author caas1
 */
class EmployeeCalculator implements TaxCalculator {
    private static final double TAX_RATE = 0.13;

  @Override
  public double calculateTax(double salary) {
    return salary * TAX_RATE;
  }
}
