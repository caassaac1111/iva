/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavenproject1;
import java.util.Scanner;
/**
 *
 * @author caas1
 */
class EmployeeTaxMenu {
  private TaxCalculator calculator;
  private Scanner scanner;

  public EmployeeTaxMenu(TaxCalculator calculator) {
    this.calculator = calculator;
    this.scanner = new Scanner(System.in);
  }

  public void run() {
    Employee employee = null;
    boolean running = true;
    while (running) {
      printMenu();
      int choice = scanner.nextInt();
      switch (choice) {
        case 1 -> employee = readEmployee();
        case 2 -> {
            if (employee != null) {
                double tax = calculator.calculateTax(employee.getSalary());
                System.out.println("Impuesto de " + employee.getName() + ": " + tax);
            } else {
                System.out.println("No se encontró datos de empleado.");
            } }
        case 3 -> running = false;
        default -> System.out.println("Opción inválida.");
      }
    }
  }

  private void printMenu() {
    System.out.println("Menú:");
    System.out.println("1. Introducir empleado");
    System.out.println("2. Calcular impuesto");
    System.out.println("3. Salir");
  }

  private Employee readEmployee() {
    System.out.println("Ingrese nombre de empleado:");
    String name = scanner.next();
    System.out.println("Ingrese edad de empleado:");
    int age = scanner.nextInt();
    System.out.println("Ingrese salario de empleado:");
    double salary = scanner.nextDouble();
    System.out.println("Ingrese cargo de empleado:");
    String occupation = scanner.next();
    return new Employee(name, age, salary, occupation);
  }
}
