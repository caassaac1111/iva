/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavenproject1;

/**
 *
 * @author caas1
 */
class Employee {
    private String name;
    private int age;
    private double salary;
    private String occupation;

    public Employee(String name, int age, double salary, String occupation) {
      this.name = name;
      this.age = age;
      this.salary = salary;
      this.occupation = occupation;
    }

    public String getName() {
      return name;
    }

    public int getAge() {
      return age;
    }

    public double getSalary() {
      return salary;
    }

    public String getOccupation() {
      return occupation;
    }
}
